<?php
/**
 * @file
 *
 * Hooks supporting queuing.
 * Just a quick auto generated list for insert/update/etc. hooks
 */

function queued_invoke_comment_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_comment_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_comment_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_comment_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_attach_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_attach_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_attach_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_attach_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_storage_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_storage_pre_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_field_storage_pre_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_filter_format_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_filter_format_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_image_style_save() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_image_style_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_menu_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_menu_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_menu_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_revision_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_type_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_type_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_node_type_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_path_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_path_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_path_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_entity_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_entity_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_entity_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_entity_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_menu_link_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_menu_link_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_menu_link_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_file_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_file_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_file_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_file_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_actions_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_vocabulary_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_vocabulary_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_vocabulary_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_vocabulary_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_term_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_term_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_term_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_taxonomy_term_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_login() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_logout() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_role_presave() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_role_insert() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_role_update() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }
function queued_invoke_user_role_delete() { queued_invoke_queue_hook( queued_invoke_convert_to_hook(__FUNCTION__), func_get_args()); }

/**
 * Get array of the hooks that Queued Invoke actually implements.
 */
function queued_invoke_get_hooks() {
  $hooks = array(
    'comment_presave',
    'comment_insert',
    'comment_update',
    'comment_delete',
    'field_presave',
    'field_insert',
    'field_update',
    'field_delete',
    'field_attach_insert',
    'field_attach_delete',
    'field_storage_delete',
    'field_storage_pre_insert',
    'field_storage_pre_update',
    'filter_format_insert',
    'filter_format_update',
    'image_style_save',
    'image_style_delete',
    'menu_insert',
    'menu_update',
    'menu_delete',
    'node_delete',
    'node_revision_delete',
    'node_insert',
    'node_presave',
    'node_update',
    'node_type_insert',
    'node_type_update',
    'node_type_delete',
    'path_insert',
    'path_update',
    'path_delete',
    'entity_presave',
    'entity_insert',
    'entity_update',
    'entity_delete',
    'menu_link_insert',
    'menu_link_update',
    'menu_link_delete',
    'file_presave',
    'file_insert',
    'file_update',
    'file_delete',
    'actions_delete',
    'taxonomy_vocabulary_presave',
    'taxonomy_vocabulary_insert',
    'taxonomy_vocabulary_update',
    'taxonomy_vocabulary_delete',
    'taxonomy_term_presave',
    'taxonomy_term_insert',
    'taxonomy_term_update',
    'taxonomy_term_delete',
    'user_delete',
    'user_presave',
    'user_insert',
    'user_update',
    'user_login',
    'user_logout',
    'user_role_presave',
    'user_role_insert',
    'user_role_update',
    'user_role_delete',
  );
  drupal_alter('queued_invoke_hooks', $hooks);
  return $hooks;
}
